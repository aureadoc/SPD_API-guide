.. _revision:

Revision History
================

v2.02 (13/02/22)
----------------

- Correct deadtime Range function error

v2.01 (24/11/22)
----------------

- Change libusb interface index to open multiple devices

v2.00 (16/07/21)
----------------

- Add Device index in all functions
- Modify all get function to return value by pointer
- Remove correlator elements
- Add ``SPD_`` at the beginning of all functions
- Replace functions :
	- SPD_listATdevices() to SPD_listDevices()
	- SPD_openATdevice() to SPD_openDevice()
	- SPD_closeATdevice() to SPD_closeDevice()
	- SPD_saveAll() to SPD_saveAllSettings()
	- SPD_getSPDversio() to SPD_getSystemVersion
- Add Wrapper
- Handle multiple device application

v1.23 (25/02/20)
----------------

- Fix bad USB frame recoverd after frequency settings

v1.22 (05/02/19)
----------------

- Rename:
	- systemFeatures() to GetSystemFeatures()

v1.21 (14/02/18)
----------------

- Improvement of SetDeadTime() function

v1.20 (29/05/17)
----------------
- Improvment of TDC functions to match new correlator

v1.19 (11/09/17)
----------------

- Decimal separator fix '.' for all functions
- Improve GetNtdcHistogramData() function

v1.18 (17/08/17)
----------------

- Add function :
	- StopATactivity()
- Improvement of the TDC calibration
- Add comments

v1.17 (10/08/17)
----------------

- Rename functions :
	- SetVisibleModuleDetectionMode() to SetModuleDetectionMode()
	- GetVisibleModuleDetectionMode() to GetModuleDetectionMode()

v1.16 (12/01/17)
----------------

Improvement of internals functions to close the software cleaner way 

v1.15 (16/12/16)
----------------

- Suppress of GetLTDCdataCh2() function
- Rename GetLTDCdataCh1() -> GetLTDCdata()
- Improvement of functions :
	- StartLTDCch()
	- StartELTDC()
	- GetLTDCdata()
	- GetELTDCdata()

v1.14 (23/11/16)
----------------

- Improvement of internal functions to recover clock & detection at speed up to 10ms
- Modification of GetIntegrationTime() and SetIntegrationTime() comments

v1.13 (27/01/16)
----------------

- Replacing functions :
	- GetCountingRate() -> GetIntegrationTime()
	- SetCountingRate() -> SetIntegrationTime()
	- GetIntegTime() -> GetIntegTimeAO()
	- SetIntegTime() -> SetIntegTimeAO()
- Improvement of functions :
	- GetCLKCountData()
	- GetClkCountModule()
- Add some comments
- Change comments :
	-invert mode value of GetVisibleModuleDetectionMode() and SetVisibleModuleDetectionMode()

v1.12 (23/12/15)
----------------

- Improvement of all functions for triple and quad modules
- Modification of functions comments 
- Modification of functions returns
- Add functions :
	- GetClkCountModule()

v1.11 (01/12/15)
----------------

- Minor internal modification

v1.10 (21/10/15)
----------------

- Improvement of StartTDCmeasures() function

v1.09 (21/08/15)
----------------

- Improvement of functions :
	- GetTdcHistogramData()
	- GetNtdcHistogramData()

v1.08 (02/03/15)
----------------

- Improvement of ListATdevices()

v1.07 (25/02/15)
----------------

- Internal features modifications

v1.06 (27/11/14)
----------------

- Add functions :
	- StartELTDC()
	- StopELTDC()
	- GetELTDCdata()
	- ListATdevices()
- Modification functions :
	- GetLTDCdataCh1()
	- GetLTDCdataCh2()
- Replacing functions:
	- OpenSPDsystem()  -> OpenATdevice()
	- CloseSPDsystem() -> CloseATdevice()

v1.05 (28/10/14)
----------------

- Suppress initial functions :
	- GetCountingMode()
	- SetCountingMode()

v1.04 (23/09/14)
----------------

- Improvement functions :
	- GetNtdcHistogramData()
	- GetsystemFeatures()

v1.03 (04/03/14)
----------------

- Improvement functions :
	- GetTdcHistogramData()
	- GetNtdcHistogramData()

v1.02 (12/02/14)
----------------

- Suppress functions :
	- GetTdcData()
- Add functions:
	- GetTdcHistogramData()
	- GetNtdcHistogramData()

v1.01 (17/01/14)
----------------

- Suppress initial functions :
	- ArmEP()
	- GetTDCdataEP()
	- StartTDCcalibration()
	- StopTDCcalibration()
- Add functions :
	- TDCinitialization()
	- GetNtdcHistogramData()
	- SetOutputFormat()
	- GetOutputFormat()
	- SetIntegTime()
	- GetIntegTime()
	- SetAnalogOutGain()
	- GetAnalogOutGain()
	- SetDetOutDelay()
	- GetDetOutDelay()

v1.00 (30/09/13)
----------------

- First release