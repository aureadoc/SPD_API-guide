# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
from sphinx.builders.html import StandaloneHTMLBuilder
import subprocess, os

# Doxygen
subprocess.call('doxygen Doxyfile.in', shell=True)


# -- Project information -----------------------------------------------------

project = 'SPD - API guide'
copyright = '2021, Aurea Technology'
author = 'Aurea Technology'

# The short X.Y version
version = '1.0.1'
# The full version, including alpha/beta/rc tags
release = '{}'.format(version)


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.autosectionlabel',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx_sitemap',
    'sphinx.ext.inheritance_diagram',
    'breathe',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

# These folders are copied to the documentation's HTML output
html_static_path = ['_static']

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    'css/custom.css',
]

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_scaled_image_link = False
html_theme = 'sphinx_rtd_theme'
html_logo = '_static/images/AUREA_LOGO.png'
html_show_sourcelink = False
html_show_sphinx = False
html_favicon = '_static/images/aurea.ico'
html_theme_options = {
    'canonical_url': '',
    'analytics_id': '',
    'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
    'logo_only': False,
    'nosidebar': True,

    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 4,
    'includehidden': True,
    'titles_only': False
}

html_context = {
    "display_github": False,
    "last_updated": True,
    "commit": False,
}

# -- Latex configuration -------------------------------------------------

# conf.py options for Latex
latex_engine = 'pdflatex'
latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    'papersize': 'a4paper',
    'releasename':" ",

    # Sonny, Lenny, Glenn, Conny, Rejne, Bjarne and Bjornstrup
    'fncychap': '\\usepackage[Sonny]{fncychap}',
    'figure_align':'H',

    # The font size ('10pt', '11pt' or '12pt').
    'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    'preamble': r'''

        %%%%%%%%%%% datetime
        \usepackage{datetime}

        \newdateformat{MonthYearFormat}{%
            \monthname[\THEMONTH], \THEYEAR}

        %%% Alternating Header for oneside
        \fancyhead[L]{\ifthenelse{\isodd{\value{page}}}{ \small \nouppercase{\leftmark} }{}}
        \fancyhead[R]{\ifthenelse{\isodd{\value{page}}}{}{ \small \nouppercase{\rightmark} }}

        %%% Alternating Footer for two side
        \fancyfoot[RO, RE]{\scriptsize Aurea Technology}

        %%% page number
        \fancyfoot[CO, CE]{\thepage}

        \renewcommand{\headrulewidth}{0.5pt}
        \renewcommand{\footrulewidth}{0.5pt}

        %%%%%%%%%%% Quote Styles at the top of chapter
        \usepackage{epigraph}
        \setlength{\epigraphwidth}{0.8\columnwidth}
        \newcommand{\chapterquote}[2]{\epigraphhead[60]{\epigraph{\textit{#1}}{\textbf {\textit{--#2}}}}}
        %%%%%%%%%%% Quote for all places except Chapter
        \newcommand{\sectionquote}[2]{{\quote{\textit{``#1''}}{\textbf {\textit{--#2}}}}}
    ''',

    'maketitle': r'''
        \begin{titlepage}
            \centering

            \vspace*{40mm} %%%
            \textbf{\Huge {SPD - API guide}}

            \vspace{20mm}
            \begin{figure}[!h]
                \centering
                \includegraphics[scale=1.50]{AUREA_LOGO_LATEX.png}
            \end{figure}

            \vspace{20mm}
            \Large \textbf{{Aurea Technology}}

            \small Created on : Octorber, 2021

            \vspace*{0mm}
            \small  Last updated : \MonthYearFormat\today
        \end{titlepage}

        \clearpage
        \pagenumbering{gobble}
        \tableofcontents
        \chapter*{Introduction} 
        \addcontentsline{toc}{chapter}{Introduction}
        \pagenumbering{arabic}

        ''',
    # Latex figure (float) alignment
    #
    # 'figure_align': 'H',
    'sphinxsetup': \
        'hmargin={2in,1.5in}, vmargin={1.5in,2in}, \
        verbatimwithframe=true, \
        TitleColor={rgb}{0.126,0.263,0.361}, \
        HeaderFamily=\\rmfamily\\bfseries, \
        InnerLinkColor={rgb}{0.208,0.374,0.486}, \
        OuterLinkColor={rgb}{0.216,0.439,0.388}',

        'tableofcontents':' ',
}

latex_logo = '_static/images/AUREA_LOGO_LATEX.png'
latex_documents = [
    (master_doc, 'SPD_API-guide.tex', 'SPD - API guide',
     'Aurea Technology', 'report')
]

# -- Breathe configuration -------------------------------------------------

breathe_projects = {
    "SPD - API guide": "_build/xml/"
}
breathe_default_project = "SPD - API guide"
breathe_default_members = ('members', 'undoc-members')