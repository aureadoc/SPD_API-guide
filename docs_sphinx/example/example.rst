.. _example:

Code Examples
=============

The following section present simple codes in C++ and Python to use SPD device.

Communication
-------------

This first example shows how to list all SPD connected to a computer and how to open and close USB communication. Device information is also recovered in this example.

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "SPD.h"

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		short ModuleNumber;
		short sValue;
		char* pch;
		char version[64];
		char versionParam[3][32];
		char systemName[6];
		char* next_pch = NULL;
		memset(version, ' ', 64);
		memset(systemName, '\0', 6);

		/*	listDevices function	*/
		// List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
		if (SPD_listDevices(devicesList, &numberDevices) == 0) {
			if (numberDevices == 0) {
				cout << endl << "	Please connect device !" << endl << endl;
				do {
					delay(500);
					SPD_listDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// If multiple SPD devices are detected, select one else open it
		if (numberDevices > 1) {
			for (int i = 0; i < numberDevices; i++) {
				printf(" -%u: %s\n", i, devicesList[i]);
			}
			cout << endl << "Select device to drive: ";
			cin >> iDev;

			if (SPD_openDevice(iDev) != 0) {
				cout << "Failed to open SPD" << endl;
			}
		}
		else {
			iDev = 0;
			if (SPD_openDevice(iDev) != 0) {
				cout << "Failed to open SPD" << endl;
			}
		}

		// System version recovery
		if (SPD_getSystemVersion(iDev, version) == 0) {			// Recovery of the system version
			cout << endl << "  * System version:" << endl << endl;
		}
		else {
			cout << endl << " -> Failed to get system version" << endl << endl;
		}

		// Parameters recovery
		int v = 0;
		pch = secure_strtok(version, ":", &next_pch);
		while (pch != NULL) {
			snprintf((char*)&versionParam[v][0], 32, "%s", pch);
			pch = secure_strtok(NULL, ":", &next_pch);
			v++;
		}
		if (pch != 0) { snprintf((char*)&versionParam[v][0], 32, "%s", pch); }

		// Show system identity
		memcpy(systemName, (char*)&versionParam[2][0] + 3, 3);
		cout << "	AT System       : " << systemName << endl;
		cout << "	Serial number   : " << versionParam[0] << endl;
		cout << "	Product number  : " << versionParam[1] << endl;
		cout << "	Firmware version: " << versionParam[2] << endl;
		cout << endl;

		// Recover SPD Features
		if (SPD_getSystemFeature(iDev, 0, &sValue) == 0) {
			switch (sValue) {
			case 0: cout << "	-> Infra Red module" << endl; break;
			case 1: cout << "	-> Biologic module" << endl; break;
			case 2: cout << "	-> Visible module" << endl; break;
			case 3: cout << "	-> Visible and Infra Red" << endl; break;
			}
		}
		else cout << " -> Failed to get system feature" << endl;

		if (SPD_getSystemFeature(iDev, 2, &ModuleNumber) == 0) {
			switch (ModuleNumber) {
			case 0: cout << "	-> Single module" << endl; break;
			case 1: cout << "	-> Dual SPD module" << endl; break;
			}
		}
		else cout << " -> Failed to get system feature" << endl;

		// Wait some time
		delay(2000);

		/*	StopActivity function	*/
		if (SPD_stopActivity(iDev) == 0) cout << "   -> activity stopped" << endl;   // Stop blocking function and data recovery pending
		else cout << "  -> Failed to stop activity" << endl;

		/*	CloseDevice function	*/
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (SPD_closeDevice(iDev) == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		return 0;
	}

Python program
~~~~~~~~~~~~~~

.. code-block:: python

	from ctypes import *
	import time

	# Import SPD wrapper file  
	import SPD_wrapper as SPD 

	# Application main
	def main():
		key = ''
		iDev = c_short(0)
		nDev = c_short()
		devList = []

		# Scan and open selected device
		devList,nDev=SPD.listDevices()
		if nDev==0:   # if no device detected, wait
			print ("No device connected, waiting...")
			while nDev==0:
			    devList,nDev=SPD.listDevices()
			    time.sleep(1)
		elif nDev>1:  # if more 1 device detected, select target
			print("Found " + str(nDev) + " device(s) :")
			for i in range(nDev):
			    print (" -"+str(i)+": " + devList[i])
			iDev=int(input("Select device to open (0 to n):")) 

		# Open device
		if SPD.openDevice(iDev)<0:
			input(" -> Failed to open device, press enter to quit !")
			return 0	
		print("Device correctly opened")

		# Recover SPD Features
		ret, type, number, correlation, detection = SPD.getSystemFeatures(iDev)
		if ret < 0: print(" -> failed\n")
		else: print(" -> {} (0 : IR, 1: Biologic, 2 : Visible, 3 : Visible & IR)\n -> {} (0 : single module, 1: dual module, 2 : triple module, 3 : quad module)\n -> {} (0 : No correlation, 1: correlation high resolution, 2 : correlation long time, 3 : correlation high resolution and long time)\n -> {} (0 : Gated detection, 1: Gated and continous detection)\n\n".format(type, number, correlation, detection))

		# Wait some time
		time.sleep(2)

		# Close device communication
		SPD.closeDevice(iDev)

	# Python main entry point
	if __name__ == "__main__":
		main() 


Recover Data
------------

The next example shows how to use SPD to recover clock and photon count from one module. You can place the function SPD_getClkCountModule in a loop in order to get multiple data. You can also use SPD_getCLKCountData which recover clock and photon count from all modules.

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "SPD.h"

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short module = 0;
		short numberDevices;
		short ModuleNumber;
		short sValue;
		unsigned long CLK1 = 0, CLK2 = 0, Count1 = 0, Count2 = 0;

		/*	listDevices function	*/
		// List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
		if (SPD_listDevices(devicesList, &numberDevices) == 0) {
			if (numberDevices == 0) {
				cout << endl << "	Please connect device !" << endl << endl;
				do {
					delay(500);
					SPD_listDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// If multiple SPD devices are detected, select one else open it
		if (numberDevices > 1) {
			for (int i = 0; i < numberDevices; i++) {
				printf(" -%u: %s\n", i, devicesList[i]);
			}
			cout << endl << "Select device to drive: ";
			cin >> iDev;

			if (SPD_openDevice(iDev) != 0) {
				cout << "Failed to open SPD" << endl;
			}
		}
		else {
			iDev = 0;
			if (SPD_openDevice(iDev) != 0) {
				cout << "Failed to open SPD" << endl;
			}
		}

		// Recover Clock and Photons count for one module
		if (SPD_getClkCountModule(iDev, module, &CLK1, &Count1) != 0) {
			cout << "! data not match !" << endl;
		}
		else {
			printf("\r	Clock %u : %7lu	Hz	Counts %u: %7lu", module + 1, CLK1, module + 1, Count1);
		}

		// Wait some time
		delay(2000);

		/*	StopActivity function	*/
		if (SPD_stopActivity(iDev) == 0) cout << "   -> activity stopped" << endl;   // Stop blocking function and data recovery pending
		else cout << "  -> Failed to stop activity" << endl;

		/*	CloseDevice function	*/
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (SPD_closeDevice(iDev) == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		return 0;
	}

Python program
~~~~~~~~~~~~~~

.. code-block:: python

	from ctypes import *
	import time

	# Import SPD wrapper file  
	import SPD_wrapper as SPD 

	# Application main
	def main():
		key = ''
		iDev = c_short(0)
		nDev = c_short()
		devList = []

		# Scan and open selected device
		devList,nDev=SPD.listDevices()
		if nDev==0:   # if no device detected, wait
			print ("No device connected, waiting...")
			while nDev==0:
			    devList,nDev=SPD.listDevices()
			    time.sleep(1)
		elif nDev>1:  # if more 1 device detected, select target
			print("Found " + str(nDev) + " device(s) :")
			for i in range(nDev):
			    print (" -"+str(i)+": " + devList[i])
			iDev=int(input("Select device to open (0 to n):")) 

		# Open device
		if SPD.openDevice(iDev)<0:
			input(" -> Failed to open device, press enter to quit !")
			return 0	
		print("Device correctly opened")

		# Wait some time
		time.sleep(2)

		# Recover Clock and Photons Count for one module
		in1 = int(input("Enter module number : "))
		ret, clk, det = SPD.getClkCountModule(iDev, in1)
		if ret < 0: print(" -> failed\n")
		else: print(" Clock     = {} Hz \n Detection = {} cnt\s \n".format(clk.value, det.value))

		# Wait some time
		time.sleep(2)

		# Close device communication
		SPD.closeDevice(iDev)

	# Python main entry point
	if __name__ == "__main__":
		main() 

.. note::

	All function information is available in section :ref:`All Functions`.
